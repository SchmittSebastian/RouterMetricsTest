import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;

public class RouteeActor extends AbstractActor {

    public Receive createReceive() {
        return ReceiveBuilder.create().match(String.class, System.err::println).build();
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("start routee " + getSelf().path().toSerializationFormat());
    }

    @Override
    public void postStop() throws Exception {
        System.out.println("stop routee " + getSelf().path().toSerializationFormat());
    }

    public static Props props() {
        return Props.create(RouteeActor.class);
    }
}
