import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.FromConfig;
import kamon.Kamon;
import kamon.kamino.KaminoReporter;

public class RouterMetricsTest {

    public static void main(String args[]) {
        ActorSystem system = ActorSystem.create("test");

        Kamon.addReporter(new KaminoReporter());

        ActorRef router = system.actorOf(FromConfig.getInstance().props(RouteeActor.props()), "router");

        router.tell("test", ActorRef.noSender());
    }
}
